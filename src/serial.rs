use crate::io;
use arduino_hal::hal::port::{PD0, PD1};
use arduino_hal::port::mode::Input;
use arduino_hal::port::mode::Output;
use arduino_hal::port::Pin;
use arduino_hal::Usart;
use avr_device::atmega328p::USART0;
use core::task::Context;
use core::task::Poll;
use embedded_hal::serial::Read;
use embedded_hal::serial::Write;

pub struct AsyncSerial<T>(T);

impl AsyncSerial<Usart<USART0, Pin<Input, PD0>, Pin<Output, PD1>>> {
    pub fn new(serial: Usart<USART0, Pin<Input, PD0>, Pin<Output, PD1>>) -> Self {
        serial.into()
    }
}

impl From<Usart<USART0, Pin<Input, PD0>, Pin<Output, PD1>>>
    for AsyncSerial<Usart<USART0, Pin<Input, PD0>, Pin<Output, PD1>>>
{
    fn from(serial: Usart<USART0, Pin<Input, PD0>, Pin<Output, PD1>>) -> Self {
        AsyncSerial(serial)
    }
}

impl io::AsyncRead for AsyncSerial<Usart<USART0, Pin<Input, PD0>, Pin<Output, PD1>>> {
    type Error = <Usart<USART0, Pin<Input, PD0>, Pin<Output, PD1>> as Read<u8>>::Error;
    fn poll_read(
        mut self: core::pin::Pin<&mut Self>,
        _cx: &mut core::task::Context<'_>,
        buf: &mut [u8],
    ) -> core::task::Poll<Result<usize, Self::Error>> {
        if let Some(ptr) = buf.first_mut() {
            match self.0.read() {
                Ok(byte) => {
                    *ptr = byte;
                    Poll::Ready(Ok(1))
                }
                Err(nb::Error::WouldBlock) => Poll::Pending,
                Err(nb::Error::Other(err)) => Poll::Ready(Err(err)),
            }
        } else {
            Poll::Ready(Ok(0))
        }
    }
}

impl io::AsyncWrite for AsyncSerial<Usart<USART0, Pin<Input, PD0>, Pin<Output, PD1>>> {
    type Error = <Usart<USART0, Pin<Input, PD0>, Pin<Output, PD1>> as Write<u8>>::Error;

    fn poll_write(
        mut self: core::pin::Pin<&mut Self>,
        _cx: &mut Context<'_>,
        buf: &[u8],
    ) -> Poll<Result<usize, Self::Error>> {
        if let Some(byte) = buf.first() {
            match self.0.write(*byte) {
                Ok(()) => Poll::Ready(Ok(1)),
                Err(nb::Error::WouldBlock) => Poll::Pending,
                Err(nb::Error::Other(err)) => Poll::Ready(Err(err)),
            }
        } else {
            Poll::Ready(Ok(0))
        }
    }

    fn poll_flush(
        mut self: core::pin::Pin<&mut Self>,
        _cx: &mut Context<'_>,
    ) -> Poll<Result<(), Self::Error>> {
        self.0.flush();
        Poll::Ready(Ok(()))
    }

    fn poll_close(
        self: core::pin::Pin<&mut Self>,
        _cx: &mut Context<'_>,
    ) -> Poll<Result<(), Self::Error>> {
        Poll::Ready(Ok(()))
    }
}
