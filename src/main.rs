#![no_std]
#![no_main]
#![feature(future_join)]

use arduino_hal::port::{mode::Output, Pin, PinOps};
use async_avr::{
    io::{AsyncReadExt, AsyncWriteExt},
    serial::AsyncSerial,
    time::Delay,
    Yield,
};
use core::{future::join, panic::PanicInfo};
use heapless::String;
use safe_regex::{regex, Matcher3};

const SERIAL_RATE: u32 = 9600;

#[arduino_hal::entry]
fn main() -> ! {
    // Safely get a reference to all the peripherals
    let dp = arduino_hal::Peripherals::take().unwrap();
    let pins = arduino_hal::pins!(dp);

    // We need to initialze the millis function globally to use it
    async_avr::time::millis_init(dp.TC0);

    // Create a serial port
    let mut serial = arduino_hal::default_serial!(dp, pins, SERIAL_RATE);
    ufmt::uwriteln!(&mut serial, "Hello from the Arduino!").unwrap();

    // A loop that reads from the serial port, then tells you if it was a valid date or not.
    let mut async_serial = AsyncSerial::from(serial);
    let dates: Matcher3<_> = regex!(br".*([0-9]{2})[-:/]([0-9]{2})[-:/]([0-9]{4}).*\n?");
    const N: usize = 32;
    let mut buf = [0; N];
    let serial_loop = async {
        loop {
            let line = async_serial.read_line(&mut buf).await.unwrap();
            async_serial.write_all(b"Got message!\n").await.unwrap();
            // if let Some((_day, _month, _year)) = dates.match_slices(&line) {
            //     async_serial.write_all(b"Got date!\n").await.unwrap();
            // } else {
            //     async_serial.write_all(b"Not date!\n").await.unwrap();
            // }
            Yield::default().await;
        }
    };

    let led1 = pins.d6.into_output();
    let led2 = pins.d7.into_output();
    let led3 = pins.d13.into_output();

    let f1 = blink(led1, 2400 / 2 / 3);
    let f2 = blink(led2, 2400 / 2 / 4);
    let f3 = blink(led3, 2400 / 2 / 5);

    #[allow(unreachable_code)] // Here because rust_analyzer incorrectly marks this
    async_avr::block_on(async { join!(f1, f2, f3, serial_loop).await });
    unreachable!();
}

/// Toggles a given pin at a set interval
async fn blink(mut pin: Pin<Output, impl PinOps>, ms: u16) -> ! {
    let mut timer = Delay::new();
    loop {
        pin.toggle();
        timer.delay(ms as u32).await;
    }
}

trait AsyncReadLine: AsyncReadExt {
    /// Read a single unicode character from the serial port.
    /// This assumes utf-8 encoding. The function does not check
    /// if the characters are actually valid unicode.
    async fn read_char<'a>(&'a mut self) -> Result<char, ()>
    where
        Self: Unpin,
    {
        let mut buf = [0u8; 4];
        self.read(&mut buf[0..1]).await.map_err(|_e| ())?;
        match buf[0].leading_ones() as usize {
            n if [0, 2, 3, 4].contains(&n) => {
                for i in 1..n {
                    self.read(&mut buf[i..i + 1]).await.map_err(|_e| ())?;
                }
                Ok(unsafe { core::mem::transmute(buf) })
            }
            _ => panic!("Invalid unicode!"),
        }
    }

    async fn read_line<'a>(&'a mut self, buf: &'a mut [u8]) -> Result<&'a [u8], ()>
    where
        Self: Unpin,
    {
        let mut tmp_buf = [0];
        for i in 0..buf.len() {
            let _ = self.read(&mut tmp_buf).await;
            let new_word = tmp_buf[0];
            buf[i] = new_word;
            if new_word == b'\n' {
                return Ok(&buf[..i + 1]);
            }
        }
        Err(())
    }

    async fn read_line_to_string<'a, const N: usize>(
        &'a mut self,
        s: &'a mut String<N>,
    ) -> Result<(), ()>
    where
        Self: Unpin,
    {
        loop {
            let c = self.read_char().await?;
            if c == '\n' {
                break Ok(());
            }
            s.push(c.into())?
        }
    }
}

impl<T: AsyncReadExt> AsyncReadLine for T {}

/// Blinks the built in led very quickly when a panic occurs.
/// Also writes to a serial port if possible
#[panic_handler]
unsafe fn panic_handler<'a, 'b>(info: &'a PanicInfo<'b>) -> ! {
    // Steal the peripherals without checking if they were owned before
    let dp = arduino_hal::Peripherals::steal();

    unsafe {
        // Set all the pins to input
        dp.PORTD.ddrd.write(|w| w.bits(0x0));
        dp.PORTB.ddrb.write(|w| w.bits(0x0));
        dp.PORTC.ddrc.write(|w| w.bits(0x0));

        // Disable pullup resistors
        dp.PORTD.portd.write(|w| w.bits(0x0));
        dp.PORTB.portb.write(|w| w.bits(0x0));
        dp.PORTC.portc.write(|w| w.bits(0x0));
    }

    // obtain an output at the build in led
    let pins = arduino_hal::pins!(dp);

    // Open a serial port to write error info
    let mut serial = arduino_hal::default_serial!(dp, pins, SERIAL_RATE);
    let _ = ufmt::uwriteln!(&mut serial, "A panic occured!");
    if let Some((file, line)) = info.location().map(|loc| (loc.file(), loc.line())) {
        let _ = ufmt::uwriteln!(&mut serial, "Location:\nfile: {}\nline: {}", file, line);
    }
    if let Some(payload) = info.payload().downcast_ref::<&str>() {
        let _ = ufmt::uwriteln!(&mut serial, "Payload:\n{}", payload);
    }

    // Then just start blinking the built in LED very fast
    let mut led = pins.d13.into_output();
    loop {
        led.toggle();
        arduino_hal::delay_ms(150);
    }
}
