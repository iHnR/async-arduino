use crate::Yield;
use avr_device::interrupt::Mutex;
use core::cell::Cell;

// Possible Values:
//
// ╔═══════════╦══════════════╦═══════════════════╗
// ║ PRESCALER ║ TIMER_COUNTS ║ Overflow Interval ║
// ╠═══════════╬══════════════╬═══════════════════╣
// ║        64 ║          250 ║              1 ms ║
// ║       256 ║          125 ║              2 ms ║
// ║       256 ║          250 ║              4 ms ║
// ║      1024 ║          125 ║              8 ms ║
// ║      1024 ║          250 ║             16 ms ║
// ╚═══════════╩══════════════╩═══════════════════╝

const PRESCALER: u32 = 64;
const TIMER_COUNTS: u32 = 250;

const MILLIS_INCREMENT: u32 = PRESCALER * TIMER_COUNTS / 16000;

static MILLIS_COUNTER: Mutex<Cell<u32>> = Mutex::new(Cell::new(0));

pub fn millis_init(tc0: arduino_hal::pac::TC0) {
    // Configure the timer for the above interval (in CTC mode)
    // and enable its interrupt.
    tc0.tccr0a.write(|w| w.wgm0().ctc());
    tc0.ocr0a.write(|w| w.bits(TIMER_COUNTS as u8));
    tc0.tccr0b.write(|w| match PRESCALER {
        8 => w.cs0().prescale_8(),
        64 => w.cs0().prescale_64(),
        256 => w.cs0().prescale_256(),
        1024 => w.cs0().prescale_1024(),
        _ => unreachable!(),
    });
    tc0.timsk0.write(|w| w.ocie0a().set_bit());

    // Reset the global millisecond counter
    avr_device::interrupt::free(|cs| {
        MILLIS_COUNTER.borrow(cs).set(u32::MAX - 3000);
    });

    // Enable interrupts globally
    unsafe { avr_device::interrupt::enable() };
}

#[avr_device::interrupt(atmega328p)]
fn TIMER0_COMPA() {
    // `free` enters a critical section.
    // This ensures no interrupts happen in this time.
    // The `cs` variable is a `CriticalSection` and has to
    // be passed to Mutex before it can be read.
    avr_device::interrupt::free(|cs| {
        let counter_cell = MILLIS_COUNTER.borrow(cs);
        let counter_value = counter_cell.get();
        counter_cell.set(counter_value.wrapping_add(MILLIS_INCREMENT));
    });
}

/// Return the number of milliseconds since the `millis_init()` function was called.
pub fn millis() -> u32 {
    avr_device::interrupt::free(|cs| MILLIS_COUNTER.borrow(cs).get())
}

/// Async delay function that works by polling the current time and waiting until the given time
/// has passed.
/// This is very imprecise
/// For more precision, save the start time of your operation, and
/// use `wait_until()` with and add an increment.
/// NOTE: If we could schedule an interrupt at some interval, we could
///       tie the Waker to it and the function would be far more efficient.
///       I think this should be possible with something similar to the
///       timer example but would probably limit the amount of delays that
///       can be used globally to about 3...
/// NOTE: Another solution that would be more flexible is to have several
///       timed interrupts. e.g: at 1ms, 10ms, and 100ms. This allows the
///       timer to tie a Waker one of the timers depending on how long it
///       has left. This would probably increase efficiency even with only
///       the 1ms interrupt.
pub async fn delay_ms(ms: u32) -> () {
    Delay::new().delay(ms).await;
}

/// A delay object that remembers its last timestamp to accomodate for drift
/// This object should also handle the (rare) case of overflow and thus should
/// be able to handle all u32 values (up to 49 days)
///
/// Note that this is still not an exact delay in the sense that it could be
/// off by any amount within the millisecond or several if the executor was
/// busy for a long time. The only guarantee is that this drift will not
/// accumulate over calls.
#[derive(Debug, Clone)]
pub struct Delay {
    stamp: u32,
}

impl Delay {
    pub fn new() -> Self {
        Self { stamp: millis() }
    }

    pub async fn delay(&mut self, ms: u32) {
        let (new_stamp, overflow) = self.stamp.overflowing_add(ms);
        while millis() < new_stamp || (overflow && millis() >= self.stamp) {
            Yield::default().await;
        }
        self.stamp = new_stamp;
    }
}

#[allow(unused)]
struct Duration {
    ms: u32,
}

#[allow(unused)]
impl Duration {
    fn from_secs(seconds: u32) -> Self {
        Self { ms: seconds * 1000 }
    }
    fn from_millis(ms: u32) -> Self {
        Self { ms }
    }
    fn from_float_secs(seconds: f32) -> Self {
        Self::from_millis((seconds * 1000.) as u32)
    }
}
