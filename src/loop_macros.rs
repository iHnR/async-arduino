/// Example usage:
/// loop_with_yield! {
///     // insert code here
/// }
#[macro_export]
macro_rules! async_loop {
    ($body:block) => {
        loop {
            Yield::default().await;
            $body
        }
    };
}

/// Example usage:
/// while_with_yield! {
///     condition,
///     // insert code here
/// }
#[macro_export]
macro_rules! async_while {
    ($condition:expr, $body:block) => {
        while $condition {
            Yield::default().await;
            $body
        }
    };
}

/// for_with_yield! {
///     item in iterable,
///     // insert code here
/// }
#[macro_export]
macro_rules! async_for {
    ($pattern:pat in $iterable:expr, $body:block) => {
        for $pattern in $iterable {
            Yield::default().await;
            $body
        }
    };
}
