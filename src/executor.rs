use core::future::Future;
use core::sync::atomic::{AtomicBool, Ordering};
use core::task::{Context, Poll, RawWaker, RawWakerVTable, Waker};
use pin_utils::pin_mut;

// NOTE `*const ()` is &AtomicBool
const VTABLE: RawWakerVTable = {
    fn clone(p: *const ()) -> RawWaker {
        RawWaker::new(p, &VTABLE)
    }
    unsafe fn wake(p: *const ()) {
        wake_by_ref(p)
    }
    unsafe fn wake_by_ref(p: *const ()) {
        // (*(p as *const VolatileCell<bool>)).set(true)
        (*(p as *const AtomicBool)).store(true, Ordering::Release)
    }
    fn drop(_: *const ()) {
        // no-op
    }
    RawWakerVTable::new(clone, wake, wake_by_ref, drop)
};

/// Spawns a task and blocks until the future resolves, returning its result.
pub fn block_on<T, F: Future<Output = T>>(task: F) -> T {
    let ready = AtomicBool::new(true);
    let waker = unsafe { Waker::from_raw(RawWaker::new(&ready as *const _ as *const (), &VTABLE)) };
    let mut context = Context::from_waker(&waker);
    pin_mut!(task);
    let mut task = task;
    loop {
        while ready.load(Ordering::Relaxed) {
            match task.as_mut().poll(&mut context) {
                Poll::Ready(val) => {
                    return val;
                }
                Poll::Pending => {
                    // ready.set(false);
                    // We do nothing here so the jobs will just be `poll`ed again
                }
            }
        }
    }
}
