use core::{
    future::Future,
    task::{Context, Poll},
};

/// This is a Future where `poll()` will always return `Pending` exactly once.
/// This can be used to introduce breaks between parts of a job where other
/// processes can take over the state machine.
pub struct Yield(bool);

impl Default for Yield {
    fn default() -> Self {
        Yield(false)
    }
}

impl Future for Yield {
    type Output = ();

    fn poll(mut self: core::pin::Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<Self::Output> {
        if self.0 {
            Poll::Ready(())
        } else {
            self.0 = true;
            Poll::Pending
        }
    }
}
